Format: 3.0 (quilt)
Source: rsyslog
Binary: rsyslog
Architecture: any
Version: 1.0.3-1
Maintainer: Chris Chewa <chrischewa@gmail.com>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 4.4.1
Build-Depends: debhelper-compat (= 12)
Package-List:
 rsyslog deb unknown optional arch=any
Checksums-Sha1:
 c50113be0730527685f4974e0ce8e8b2fec97a84 112588 rsyslog_1.0.3.orig.tar.xz
 9c061e94697dd2ef7226456b41677b9e75374905 7876 rsyslog_1.0.3-1.debian.tar.xz
Checksums-Sha256:
 3d77a2359c301892aa56f4877ce2e4232dac71669644f7ae4b0b6c66d43911f4 112588 rsyslog_1.0.3.orig.tar.xz
 0524ebdf97955e7bd6b109aeb7ad5b08583e5b702c5bd6512a984331a771462b 7876 rsyslog_1.0.3-1.debian.tar.xz
Files:
 5d6d764ce375e915a7957c784aea461e 112588 rsyslog_1.0.3.orig.tar.xz
 2807db5c3d64c98829ddff099bb4bcc2 7876 rsyslog_1.0.3-1.debian.tar.xz
