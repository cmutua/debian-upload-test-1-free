#!/bin/bash

export DEBEMAIL="cmtua@gitlab.com"
export DEBFULLNAME="cmutua"
export LOGNAME="cmutua"

cat <<EOF > dput.cf
[gitlab]
method = https
fqdn = gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com
incoming = /api/v4/projects/${CI_PROJECT_ID}/packages/debian
EOF

dput --config=dput.cf --unchecked --no-upload-log gitlab rsyslog_1.0.3-1_amd64.changes
