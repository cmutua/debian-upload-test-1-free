.\" Copyright 2004-2005 Rainer Gerhards and Adiscon for the rsyslog modifications
.\" May be distributed under the GNU General Public License
.\"
.TH RSYSLOGD 8 "12 September 2005" "Version 1.0.1" "Linux System Administration"
.SH NAME
rsyslogd \- reliable and extended syslogd 
.SH SYNOPSIS
.B rsyslogd
.RB [ " \-a "
.I socket
]
.RB [ " \-d " ]
.RB [ " \-f "
.I config file
]
.RB [ " \-h " ] 
.RB [ " \-i "
.I pid file
]
.RB [ " \-l "
.I hostlist
]
.RB [ " \-m "
.I interval
] 
.RB [ " \-n " ]
.RB [ " \-o " ]
.RB [ " \-p"
.IB socket 
]
.RB [ " \-r "
.I port
]
.br
.RB [ " \-s "
.I domainlist
]
.RB [ " \-t "
.I port
]
.RB [ " \-v " ]
.LP
.SH DESCRIPTION
.B Rsyslogd
is a system utility providing support for message logging.
Support of both internet and
unix domain sockets enables this utility to support both local
and remote logging (via UDP and TCP).

.BR Rsyslogd (8)
is derived from the sysklogd package which in turn is derived from the
stock BSD sources.

.B Rsyslogd
provides a kind of logging that many modern programs use.  Every logged
message contains at least a time and a hostname field, normally a
program name field, too, but that depends on how trusty the logging
program is. The rsyslog package supports free definition of output formats
via templates. It also supports precise timestamps and writing directly
to MySQL databases. If the database option is used, tools like phpLogCon can
be used to view the log data.

While the
.B rsyslogd
sources have been heavily modified a couple of notes
are in order.  First of all there has been a systematic attempt to
insure that rsyslogd follows its default, standard BSD behavior. Of course,
some configuration file changes are necessary in order to support the
template system. However, rsyslogd should be able to use a standard
syslog.conf and act like the orginal syslogd. However, an original syslogd
will not work correctly with a rsyslog-enhanced configuration file. At
best, it will generate funny looking file names.
The second important concept to note is that this version of rsyslogd
interacts transparently with the version of syslog found in the
standard libraries.  If a binary linked to the standard shared
libraries fails to function correctly we would like an example of the
anomalous behavior.

The main configuration file
.I /etc/rsyslog.conf
or an alternative file, given with the 
.B "\-f"
option, is read at startup.  Any lines that begin with the hash mark
(``#'') and empty lines are ignored.  If an error occurs during parsing
the error element is ignored. It is tried to parse the rest of the line.

For details and configuration examples, see the
.B rsyslog.conf (5)
man page.

.LP
.SH OPTIONS
.TP
.BI "\-a " "socket"
Using this argument you can specify additional sockets from that
.B rsyslogd
has to listen to.  This is needed if you're going to let some daemon
run within a chroot() environment.  You can use up to 19 additional
sockets.  If your environment needs even more, you have to increase
the symbol
.B MAXFUNIX
within the syslogd.c source file.  An example for a chroot() daemon is
described by the people from OpenBSD at
http://www.psionic.com/papers/dns.html.
.TP
.B "\-d"
Turns on debug mode.  Using this the daemon will not proceed a 
.BR fork (2)
to set itself in the background, but opposite to that stay in the
foreground and write much debug information on the current tty.  See the
DEBUGGING section for more information.
.TP
.BI "\-f " "config file"
Specify an alternative configuration file instead of
.IR /etc/rsyslog.conf ","
which is the default.
.TP
.BI "\-h "
By default rsyslogd will not forward messages it receives from remote hosts.
Specifying this switch on the command line will cause the log daemon to
forward any remote messages it receives to forwarding hosts which have been
defined.
.TP
.BI "\-i " "pid file"
Specify an alternative pid file instead of the default one.
This option must be used if multiple instances of rsyslogd should
run on a single machine.
.TP
.BI "\-l " "hostlist"
Specify a hostname that should be logged only with its simple hostname
and not the fqdn.  Multiple hosts may be specified using the colon
(``:'') separator.

Note: At the moment, this option is only available for command
line comptability. It has, however, NO effect and is ignored.
.TP
.BI "\-m " "interval"
The
.B rsyslogd
logs a mark timestamp regularly.  The default
.I interval
between two \fI-- MARK --\fR lines is 20 minutes.  This can be changed
with this option.  Setting the
.I interval
to zero turns it off entirely.

Note: At the moment, this option is only available for command
line comptability. It has, however, NO effect and is ignored.
.TP
.B "\-n"
Avoid auto-backgrounding.  This is needed especially if the
.B rsyslogd
is started and controlled by
.BR init (8).
.TP
.B "\-o"
Omit reading the standard local log socket. This option is most
useful for running multiple instances of rsyslogd on a single 
machine. When specified, no local log socket is opened at all.
.TP
.BI "\-p " "socket"
You can specify an alternative unix domain socket instead of
.IR /dev/log "."
.TP
.BI "\-r " "port"
Activates the syslog/udp listener service. The listener
will listen to the specified port. Please note that a 
port must be specified in any case. This is different from
the stock sysklogd package. If you would like to use
the system's default port, specify 0 as the port number. That
will result in an /etc/services lookup for the actual port
number. If the "-r" option is not given, no syslog/udp listner
is available.
.TP
.BI "\-s " "domainlist"
Specify a domainname that should be stripped off before
logging.  Multiple domains may be specified using the colon (``:'')
separator.
Please be advised that no sub-domains may be specified but only entire
domains.  For example if
.B "\-s north.de"
is specified and the host logging resolves to satu.infodrom.north.de
no domain would be cut, you will have to specify two domains like:
.BR "\-s north.de:infodrom.north.de" .
.TP
.BI "\-t " "port"
Activates the syslog/tcp listener service. The listener will listen to
the specified port. Please note that syslog/tcp is not standardized,
but the implementation in rsyslogd follows common practice and is
compatible with e.G. Cisco PIX, syslog-ng and MonitorWare (Windows).
.TP
.B "\-v"
Print version and exit.
.LP
.SH SIGNALS
.B Rsyslogd
reacts to a set of signals.  You may easily send a signal to
.B rsyslogd
using the following:
.IP
.nf
kill -SIGNAL `cat /var/run/rsyslogd.pid`
.fi
.PP
.TP
.B SIGHUP
This lets
.B rsyslogd
perform a re-initialization.  All open files are closed, the
configuration file (default is 
.IR /etc/rsyslog.conf ")"
will be reread and the
.BR rsyslog (3)
facility is started again.
.TP
.B SIGTERM
.B Rsyslogd
will die.
.TP
.BR SIGINT ", " SIGQUIT
If debugging is enabled these are ignored, otherwise 
.B rsyslogd
will die.
.TP
.B SIGUSR1
Switch debugging on/off.  This option can only be used if
.B rsyslogd
is started with the
.B "\-d"
debug option.
.TP
.B SIGCHLD
Wait for childs if some were born, because of wall'ing messages.
.LP
.SH SUPPORT FOR REMOTE LOGGING
.B Rsyslogd
provides network support to the syslogd facility.
Network support means that messages can be forwarded from one node
running rsyslogd to another node running rsyslogd (or a
compatible syslog implementation) where they will be
actually logged to a disk file.

To enable this you have to specify either the
.B "\-r"
or
.B "\-t"
option on the command line.  The default behavior is that
.B rsyslogd
won't listen to the network. You can also combine these two
options if you want rsyslogd to listen to both TCP and UDP
messages.

The strategy is to have rsyslogd listen on a unix domain socket for
locally generated log messages.  This behavior will allow rsyslogd to
inter-operate with the syslog found in the standard C library.  At the
same time rsyslogd listens on the standard syslog port for messages
forwarded from other hosts.  To have this work correctly the
.BR services (5)
files (typically found in
.IR /etc )
must have the following
entry:
.IP
.nf
	syslog          514/udp
.fi
.PP
If this entry is missing
.B rsyslogd
will use the well known port of 514 (so in most cases, it's not
really needed).

To cause messages to be forwarded to another host replace
the normal file line in the
.I rsyslog.conf
file with the name of the host to which the messages is to be sent
prepended with an @ (for UDP delivery) or the sequence @@ (for
TCP delivery). The host name can also be followed by a colon and
a port number, in which case the message is sent to the specified
port on the remote host.
.IP
For example, to forward
.B ALL
messages to a remote host use the
following
.I rsyslog.conf
entry:
.IP
.nf
	# Sample rsyslogd configuration file to
	# messages to a remote host forward all.
	*.*			@hostname
.fi
More samples can be found in sample.conf.

If the remote hostname cannot be resolved at startup, because the
name-server might not be accessible (it may be started after rsyslogd)
you don't have to worry.
.B Rsyslogd
will retry to resolve the name ten times and then complain.  Another
possibility to avoid this is to place the hostname in
.IR /etc/hosts .

With normal
.BR syslogd s
you would get syslog-loops if you send out messages that were received
from a remote host to the same host (or more complicated to a third
host that sends it back to the first one, and so on).

To avoid this no messages that were received from a
remote host are sent out to another (or the same) remote host. You can
disable this feature by the
.B \-h
option.

If the remote host is located in the same domain as the host, 
.B rsyslogd
is running on, only the simple hostname will be logged instead of
the whole fqdn.

In a local network you may provide a central log server to have all
the important information kept on one machine.  If the network consists
of different domains you don't have to complain about logging fully
qualified names instead of simple hostnames.  You may want to use the
strip-domain feature
.B \-s
of this server.  You can tell
.B rsyslogd
to strip off several domains other than the one the server is located
in and only log simple hostnames.

Using the
.B \-l
option there's also a possibility to define single hosts as local
machines.  This, too, results in logging only their simple hostnames
and not the fqdns.

.SH OUTPUT TO DATABASES
.B Rsyslogd
has support for writing data to MySQL database tables. The exact specifics
are described in the
.B rsyslog.conf (5)
man page. Be sure to read it if you plan to use database logging.

While it is often handy to have the data in a database, you must be aware
of the implications. Most importantly, database logging takes far
longer than logging to a text file. A system that can handle a large
log volume when writing to text files can most likely not handle
a similar large volume when writing to a database table.

.SH OUTPUT TO NAMED PIPES (FIFOs)
.B Rsyslogd
has support for logging output to named pipes
(fifos).  A fifo or named pipe can be used as a destination for log
messages by prepending a pipy symbol (``|'') to the name of the
file.  This is handy for debugging.  Note that the fifo must be created
with the mkfifo command before
.B rsyslogd
is started.
.IP
The following configuration file routes debug messages from the
kernel to a fifo:
.IP
.nf
	# Sample configuration to route kernel debugging
	# messages ONLY to /usr/adm/debug which is a
	# named pipe.
	kern.=debug			|/usr/adm/debug
.fi
.LP
.SH INSTALLATION CONCERNS
There is probably one important consideration when installing
rsyslogd.  It is dependent on proper
formatting of messages by the syslog function.  The functioning of the
syslog function in the shared libraries changed somewhere in the
region of libc.so.4.[2-4].n.  The specific change was to
null-terminate the message before transmitting it to the 
.I /dev/log
socket.  Proper functioning of this version of rsyslogd is dependent on
null-termination of the message.

This problem will typically manifest itself if old statically linked
binaries are being used on the system.  Binaries using old versions of
the syslog function will cause empty lines to be logged followed by
the message with the first character in the message removed.
Relinking these binaries to newer versions of the shared libraries
will correct this problem.

The
.BR rsyslogd (8)
can be run from
.BR init (8)
or started as part of the rc.*
sequence.  If it is started from init the option \fI\-n\fR must be set,
otherwise you'll get tons of syslog daemons started.  This is because 
.BR init (8)
depends on the process ID.
.LP
.SH SECURITY THREATS
There is the potential for the rsyslogd daemon to be
used as a conduit for a denial of service attack.
A rogue program(mer) could very easily flood the rsyslogd daemon with
syslog messages resulting in the log files consuming all the remaining
space on the filesystem.  Activating logging over the inet domain
sockets will of course expose a system to risks outside of programs or
individuals on the local machine.

There are a number of methods of protecting a machine:
.IP 1.
Implement kernel firewalling to limit which hosts or networks have
access to the 514/UDP socket.
.IP 2.
Logging can be directed to an isolated or non-root filesystem which,
if filled, will not impair the machine.
.IP 3.
The ext2 filesystem can be used which can be configured to limit a
certain percentage of a filesystem to usage by root only.  \fBNOTE\fP
that this will require rsyslogd to be run as a non-root process.
\fBALSO NOTE\fP that this will prevent usage of remote logging since
rsyslogd will be unable to bind to the 514/UDP socket.
.IP 4.
Disabling inet domain sockets will limit risk to the local machine.
.IP 5.
Use step 4 and if the problem persists and is not secondary to a rogue
program/daemon get a 3.5 ft (approx. 1 meter) length of sucker rod*
and have a chat with the user in question.

Sucker rod def. \(em 3/4, 7/8 or 1in. hardened steel rod, male
threaded on each end.  Primary use in the oil industry in Western
North Dakota and other locations to pump 'suck' oil from oil wells.
Secondary uses are for the construction of cattle feed lots and for
dealing with the occasional recalcitrant or belligerent individual.
.SS Message replay and spoofing
If remote logging is enabled, messages can easily be spoofed and replayed.
As the messages are transmitted in clear-text, an attacker might use
the information obtained from the packets for malicious things. Also, an
attacker might reply recorded messages or spoof a sender's IP address,
which could lead to a wrong preception of system activity. Be sure to think
about syslog network security before enabling it.
.LP
.SH DEBUGGING
When debugging is turned on using
.B "\-d"
option then
.B rsyslogd
will be very verbose by writing much of what it does on stdout.  Whenever
the configuration file is reread and re-parsed you'll see a tabular,
corresponding to the internal data structure.  This tabular consists of
four fields:
.TP
.I number
This field contains a serial number starting by zero.  This number
represents the position in the internal data structure (i.e. the
array).  If one number is left out then there might be an error in the
corresponding line in
.IR /etc/rsyslog.conf .
.TP
.I pattern
This field is tricky and represents the internal structure
exactly.  Every column stands for a facility (refer to
.BR syslog (3)).
As you can see, there are still some facilities left free for former
use, only the left most are used.  Every field in a column represents
the priorities (refer to
.BR syslog (3)).
.TP
.I action
This field describes the particular action that takes place whenever a
message is received that matches the pattern.  Refer to the
.BR syslog.conf (5)
manpage for all possible actions.
.TP
.I arguments
This field shows additional arguments to the actions in the last
field.  For file-logging this is the filename for the logfile; for
user-logging this is a list of users; for remote logging this is the
hostname of the machine to log to; for console-logging this is the
used console; for tty-logging this is the specified tty; wall has no
additional arguments.
.TP
.SS templates
There will also be a second internal structure which lists all
defined templates and there contents. This also enables you to see
the internally-defined, hardcoded templates.
.SH FILES
.PD 0
.TP
.I /etc/rsyslog.conf
Configuration file for
.BR rsyslogd .
See
.BR rsyslog.conf (5)
for exact information.
.TP
.I /dev/log
The Unix domain socket to from where local syslog messages are read.
.TP
.I /var/run/rsyslogd.pid
The file containing the process id of 
.BR rsyslogd .
.PD
.SH BUGS
This is an early release of
.B Rsyslogd .
As such, there are probably a number of bugs. Those that I know
are described in the file BUGS that came with the package. Be sure
to review it.

If an error occurs in one line the whole rule is ignored.

.B Rsyslogd
doesn't change the filemode of opened logfiles at any stage of
process.  If a file is created it is world readable.  If you want to
avoid this, you have to create it and change permissions on your own.
This could be done in combination with rotating logfiles using the 
.BR savelog (8)
program that is shipped in the 
.B smail
3.x distribution.  Remember that it might be a security hole if
everybody is able to read auth.* messages as these might contain
passwords.
.LP
.SH SEE ALSO
.BR rsyslog.conf (5),
.BR logger (1),
.BR syslog (2),
.BR syslog (3),
.BR services (5),
.BR savelog (8)
.LP
.SH COLLABORATORS
.B rsyslogd
is derived from sysklogd sources, which in turn was taken from
the BSD sources. Special thanks to Greg Wettstein (greg@wind.enjellic.com)
and Martin Schulze (joey@linux.de) for the fine sysklogd package.

.PD 0
.TP
Rainer Gerhards
.TP
Adiscon GmbH
.TP
Grossrinderfeld, Germany
.TP
rgerhards@adiscon.com

.TP
Michael Meckelein
.TP
Adiscon GmbH
.TP
mmeckelein@adiscon.com
.PD
.zZ
