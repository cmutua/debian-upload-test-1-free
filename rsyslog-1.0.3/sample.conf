# This is a sample configuation file for rsyslogd. See the
# man pages for details. Please note that rsyslogd by default
# reads /etc/rsyslogd.conf (and NOT /etc/syslogd.conf!).
#
# A commented sample configuration. More a man page than a real
# sample ;)
#
# We try to keep things as consistent with existing syslog implementation
# as possible. We use "$" to start lines that contain new directives.
# Set syslogd options

# Templates are a key feature of rsyslog. They allow to specify any
# format a user might want. Every output in rsyslog uses templates - this
# holds true for files, user messages and so on. The database writer
# expects its template to be a proper SQL statement - so this is highly
# customizable too. You might ask how does all of this work when no templates
# at all are specified. Good question ;) The answer is simple, though. Templates
# compatible with the stock syslogd formats are hardcoded into rsyslog. So if
# no template is specified, we use one of these hardcoded templates. Search for
# "template_" in syslogd.c and you will find the hardcoded ones.
#
# A template consists of a template directive, a name, the actual template text
# and optional options. A sample is:
#
# $template MyTemplateName,"\7Text %property% some more text\n",<options>
#
# The "$template" is the template directive. It tells rsyslog that this
# line contains a template.
#
# "MyTemplateName" is the template name. All other config lines refer to
# this name.
#
# The text within quotes is the actual template text. The backslash is 
# a escape character, much as in C. It does all these "cool" things. For
# example, \7 rings the bell (this is an ASCII value), \n is a new line.
# C programmers and perl coders have the advantage of knowing this, but the
# set in rsyslog is a bit restricted currently. All text in the template
# is used literally, except for things within percent signs. These are 
# properties and allow you access to the contents of the syslog message.
# Properties are accessed via the property replacer (nice name, huh) and
# it can do cool things, too. For example, it can pick a substring or
# do date-specific formatting. More on this is below, on some lines of the
# property replacer.
#
# The <options> part is optional. It carries options that influence the
# template as whole. Details are below. Be sure NOT to mistake template
# options with property options - the later ones are processed by the
# property replacer and apply to a SINGLE property, only (and not the
# whole template).
#
# Template options are case-insensitive. Currently defined are:
# sql - format the string suitable for a SQL statement. This will replace single
#       quotes ("'") by two single quotes ("''") inside each field. This option MUST
#       be specified when a template is used for writing to a database, otherwise SQL
#       injection might occur.
#
# 	Please note that the database writer *checks* that the sql option is
#	present in the template. If it is not present, the write database action
#	is disabled. This is to guard you against accidential forgetting it and
#	then becoming vulnerable for SQL injection.
#	The sql option can also be useful with files - especially if you want
#	to run them on another machine for performance reasons. However, do NOT
#	use it if you do not have a real need for it - among others, it takes
#	some toll on the processing time. Not much, but on a really busy system
#	you might notice it ;)
#
# To escape:
# % = \%
# \ = \\
# --> '\' is used to escape (as in C)
#$template TraditionalFormat,%timegenerated% %HOSTNAME% %syslogtag%%msg%\n"
#
# Properties can be accessed by the property replacer. They are accessed
# inside the template by putting them between percent signs. Properties
# can be modifed by the property replacer. The full syntax is as follows:
#
# %propname:fromChar:toChar:options%
#
# propname is the name of the property to access. This IS case-sensitive!
# Currently supported are:
# msg		the MSG part of the message (aka "the message" ;))
# rawmsg	the message excactly as it was received from the
#		socket. Should be useful for debugging.
# UxTradMsg	will disappear soon - do NOT use!
# HOSTNAME	hostname from the message
# source	alias for HOSTNAME
# syslogtag	TAG from the message
# PRI		PRI part of the message - undecoded (single value)
# IUT		the monitorware InfoUnitType - used when talking to a
#		MonitorWare backend (also for phpLogCon)
# syslogfacility	the facility from the message - in numerical form
# syslogpriority	the priority (actully severity!) from the
#			message - in numerical form
# timegenerated	timestamp when the message was RECEIVED. Always in high
#		resolution
# timereported	timestamp from the message. Resolution depends on what
#		was provided in the message (in most cases, only seconds)
# TIMESTAMP	alias for timereported
#
# FromChar and toChar are used to build substrings. They specify the
# offset within the string that should be copied. Offset counting
# starts at 1, so if you need to obtain the first 2 characters of the
# message text, you can use this syntax: "%msg:1:2%".
# If you do not whish to specify from and to, but you want to
# specify options, you still need to include the colons. For example,
# if you would like to convert the full message text to lower case
# only, use "%msg:::lowercase%".
#
# property options are case-insensitive, currently defined are:
# uppercase	convert property to lowercase only
# lowercase	convert property text to uppercase only
# drop-last-lf	The last LF in the message (if any), is dropped. 
#		Especially useful for PIX.
# date-mysql	format as mysql date
# date-rfc3164	format as RFC 3164 date
# date-rfc3339	format as RFC 3339 date
# escape-cc	NOT yet implemented

# Below find some samples of what a template can do. Have a good
# time finding out what they do [or just tun them] ;)

# A template that resambles traditional syslogd file output:
$template TraditionalFormat,"%timegenerated% %HOSTNAME% %syslogtag%%msg:::drop-last-lf%\n"

# A template that tells you a little more about the message:
$template precise,"%syslogpriority%,%syslogfacility%,%timegenerated:::fulltime%,%HOSTNAME%,%syslogtag%,%msg%\n"

# A template that resembles RFC 3164 on-the-wire format:
# (yes, there is NO space betwen syslogtag and msg! that's important!)
$template RFC3164fmt,"<%PRI%>%TIMESTAMP% %HOSTNAME% %syslogtag%%msg%"

# a template resembling traditional wallmessage format:
$template wallmsg,"\r\n\7Message from syslogd@%HOSTNAME% at %timegenerated% ...\r\n %syslogtag%%msg%\n\r"

# The template below emulates winsyslog format, but we need to check the time
# stamps used. for now, it is good enough ;) This format works best with
# other members of the MonitorWare product family. It is also a good sample
# where you can see the property replacer in action.
$template WinSyslogFmt,"%HOSTNAME%,%timegenerated:1:10:date-rfc3339%,%timegenerated:12:19:date-rfc3339%,%timegenerated:1:10:date-rfc3339%,%timegenerated:12:19:date-rfc3339%,%syslogfacility%,%syslogpriority%,%syslogtag%%msg%\n"

# A template used for database writing (notice it *is* an actual
# sql-statement):
$template dbFormat,"insert into SystemEvents (Message, Facility,FromHost, Priority, DeviceReportedTime, ReceivedAt, InfoUnitID, SysLogTag) values ('%msg%', %syslogfacility%, '%HOSTNAME%',%syslogpriority%, '%timereported:::date-mysql%', '%timegenerated:::date-mysql%', %iut%, '%syslogtag%')",sql

# Selector lines are somewhat different from stock syslogd. With
# rsyslog, you can add a semicolon ";" after the target and then
# the template name. That will assign this template to the respective
# action. If no template name is given, a hardcoded template is used.
# If a template name is given, but the template was not defined, the
# selector line is DEACTIVATED.
#
# #############
# # IMPORTANT #
# #############
# Templates MUST be defined BEFORE they are used! It is OK to
# intermix template definitions and selector lines within the
# config file, but each template MUST be defined before it is
# used the first time!
#

# We have some very rough samples here - This sample file focusses
# on the new syntax, so we do NOT describe all possible selections.
# Use the syslog.conf if you are interested to see how to select
# based on facility and severits (aka priority).

*.*		/var/log/winsyslog-like.log;WinSyslogFmt

# A selector using the traditional format defined above:
*.*		/var/log/traditionalfile.log;TraditionalFormat

# And another one using the hardcoded traditional format:
*.*		/var/log/anothertraditionalfile.log

# Templates are also fully supportd for forwarding:
*.*		@172.19.2.16;RFC3164fmt

# And this finally is a database action
# The semicolon at the end is theoreticaly not necessary,
# but the current rsyslogd has a bug that makes it abort if
# it is missing. As Dennis Olvany pointed out, it would e
# extremely nice to have this semicolon in the sample conf ;)
*.*		>hostname,dbname,userid,password;
# It uses the default schema (MonitorWare format). The parameters
# should be self-explanatory.

# And this one uses the template defined above:
*.*		>hostname,dbname,userid,password;dbFormat


#
# Rsyslog supports TCP-based syslog. To enable receiving TCP messages,
# use the -t <port> command line option (where port is the port it
# shall listen to. To forward messages to the remote host, you must
# specify a forwarding action and include the host and port. TCP
# and UDP-based forwarding has basically the same syntax, except that
# TCP delivery is triggered by specifying a second at-sign (@) in the
# message.
# This is UDP forwarding to port 514:
*.*		@172.19.2.16
# This is UDP forwarding to port 1514:
*.*		@172.19.2.16:1514
# This is TCP forwarding to port 1514:
*.*		@@172.19.2.16:1514
# The second @-sign is all you need (except, of course, a tcp-capable
# syslogd like rsyslogd ;)).
# Of course, you can also specify a template with TCP:
*.*		@@172.19.2.16:1514;RFC3164Fmt


# 
# A final world. rsyslog is considered a part of Adiscon's MonitorWare product line.
# As such, you can find current information as well as information on the
# other product line members on http://www.monitorware.com. Please be warned, there
# are a number of closed-source commercial Windows applications among these products ;)
#
# You might want to check the GPL'ed phpLogCon (http://www.phplogcon.org)
# as a web-based front-end to a syslog message database.
#
# I hope this work is useful.
# 2005-07-20 Rainer Gerhards <rgerhards@adiscon.com>
#
